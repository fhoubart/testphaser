/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var game = new Phaser.Game(1000, 600, Phaser.CANVAS, '', { preload: preload, create: create, update: update });

var numbers = [];
var cats = [];
var sounds = {};
var gameVars = {
    params: {
        delayBetweenTwoCats: 400
    }
};
function preload() {
    game.load.image('background','assets/background.png');
    game.load.spritesheet('cat', 'assets/cat.png', 35,30);
    game.load.spritesheet('dude','assets/dude.png', 32, 48);
    game.load.image('number1','assets/1.png');
    game.load.image('number2','assets/2.png');
    game.load.image('number3','assets/3.png');
    game.load.image('number4','assets/4.png');
    game.load.image('number5','assets/5.png');
    game.load.image('number6','assets/6.png');
    game.load.image('number7','assets/7.png');
    game.load.image('number8','assets/8.png');
    game.load.image('number9','assets/9.png');
    
    game.load.audio('backgroundMusic','assets/brap-brap-brap.mp3');
    game.load.audio('miaou','assets/miaou.mp3');
}


function create() {
    game.physics.startSystem(Phaser.Physics.ARCAD);
    
    var background = game.add.sprite(0,0,'background');
    
    sounds.background = game.add.audio('backgroundMusic');
    sounds.background.play();
    
    sounds.miaou = game.add.audio('miaou');
    
    var numbersGroup = game.add.group()
    numbersGroup.enableBody = true;
    
    var arrayNumbers = shuffle([1,2,3,4,5,6,7,8,9]);
    
    
    for(i=0; i<9; i++) {
        var number = numbersGroup.create(50+i/2*200,50+200*(i%2),'number'+arrayNumbers[i]);
        number.scale.setTo(0.5,0.5);
        number.inputEnabled = true;
        //number.input.pixelPerfectClick = true;
        number.events.onInputDown.add(numberClicked,this);
        numbers[arrayNumbers[i]] = number;
    }

    startRound();
}


function createCat(i) {
    var cat = game.add.sprite(1000,-100*i,'cat');
    cat.scale.setTo(2,2);
    cat.animations.add('walk',[0,1,2]);
    cat.animations.add('stand',[3,4,5]);
    cat.animations.play('walk',10,true);
    game.physics.arcade.enable(cat);
    cat.body.gravity.y = 1000;
    cat.body.collideWorldBounds = true;
    
    cat.inputEnabled = true;
    cat.events.onInputDown.add(catClicked,this);
    return cat;
}

function startRound() {
   gameVars.startTime = (new Date()).getTime();
   gameVars.nbCats = Math.floor(Math.random()*10);
   
}
function catClicked(sprite) {
    sprite.body.velocity.y = -300;
    sounds.miaou.play();
}

function numberClicked(sprite, pointer) {
    null.error = 1;
    alert("clicked ");
    cats[0].body.bounce.y = 10;
}

function update() {
    // Check if cats should become visibles
    if(cats.length<gameVars.nbCats) {
        var elapsedtime = (new Date()).getTime() - gameVars.startTime;
        for(i=cats.length; i<gameVars.nbCats; i++) {
           if(elapsedtime > gameVars.params.delayBetweenTwoCats*i) {
               cats[i] = createCat(i);
           }
        }
    }

    for(i=1; i<=9; i++) {
        //numbers[i].y += 4;
    }
    for(i in cats) {
        var cat = cats[i];
        if(cat.body.blocked.down && cat.x > 400+55*i) {
            cat.animations.play('walk');
            cat.x -= 4;
            if(cat.x < 400+55*i) cat.x = 400+55*i;
        } else {
            cat.animations.play('stand',10,true);
        }
    }
}





function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}